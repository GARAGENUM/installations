# INSTALLATIONS

Scripts d'installation des logiciels de base pour Debian 12 :cyclone:

## PRE REQUIS :paperclip:

- Operating system: Debian 12 (Bookworm)
- System version : 64 bits

## SCRIPTS :page_with_curl:

- VScodium
- Tabby
- Virtualbox
- Vagrant
- Element client
- Docker + compose + buildx
- K8s
- Terraform / OpenTofu
- Jenkins


