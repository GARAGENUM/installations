
#!/bin/bash

# UPDATE
echo "\n
██╗   ██╗██████╗ ██████╗  █████╗ ████████╗███████╗
██║   ██║██╔══██╗██╔══██╗██╔══██╗╚══██╔══╝██╔════╝
██║   ██║██████╔╝██║  ██║███████║   ██║   █████╗  
██║   ██║██╔═══╝ ██║  ██║██╔══██║   ██║   ██╔══╝  
╚██████╔╝██║     ██████╔╝██║  ██║   ██║   ███████╗
 ╚═════╝ ╚═╝     ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚══════╝"
sudo apt-get update -y

echo "\n
██╗   ██╗██████╗  ██████╗ ██████╗  █████╗ ██████╗ ███████╗
██║   ██║██╔══██╗██╔════╝ ██╔══██╗██╔══██╗██╔══██╗██╔════╝
██║   ██║██████╔╝██║  ███╗██████╔╝███████║██║  ██║█████╗  
██║   ██║██╔═══╝ ██║   ██║██╔══██╗██╔══██║██║  ██║██╔══╝  
╚██████╔╝██║     ╚██████╔╝██║  ██║██║  ██║██████╔╝███████╗
 ╚═════╝ ╚═╝      ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═════╝ ╚══════╝"
sudo apt-get upgrade -y

# PAQUETS BASIQUES
echo "\n
██████╗  █████╗ ███████╗██╗ ██████╗ ██╗   ██╗███████╗███████╗
██╔══██╗██╔══██╗██╔════╝██║██╔═══██╗██║   ██║██╔════╝██╔════╝
██████╔╝███████║███████╗██║██║   ██║██║   ██║█████╗  ███████╗
██╔══██╗██╔══██║╚════██║██║██║▄▄ ██║██║   ██║██╔══╝  ╚════██║
██████╔╝██║  ██║███████║██║╚██████╔╝╚██████╔╝███████╗███████║
╚═════╝ ╚═╝  ╚═╝╚══════╝╚═╝ ╚══▀▀═╝  ╚═════╝ ╚══════╝╚══════╝"
sudo apt-get install git wget curl net-tools iptables resolvconf rsyslog nmap python3-pip python3-venv zip openssh-server build-essential btop gimp fail2ban wireguard -y

# paramètrage de resolv.conf
echo "nameserver 1.1.1.1" | sudo tee -a /etc/resolvconf/resolv.conf.d/base
echo "nameserver 8.8.8.8" | sudo tee -a /etc/resolvconf/resolv.conf.d/base
sudo resolvconf -u

# COMMENTER LES LIGNES DES INSTALLATIONS NON VOULUES:

# BASHRC INSTALL
./scripts/bashrc_install.sh

# VSCODIUM
./scripts/vscodium_install.sh

# TABBY
./scripts/tabby_install.sh

# DOCKER
./scripts/docker_install.sh

# VIRTUALBOX
# ./scripts/virtualbox_install.sh

# VAGRANT
# ./scripts/vagrant_install.sh

# JENKINS
#./scripts/jenkins_install.sh

# KUBERNETES
#./scripts/kubernetes_install.sh

# CMATRIX SCREENSAVER
./scripts/cmatrix_install.sh

echo "\n
██████╗  ██████╗ ███╗   ██╗███████╗██╗
██╔══██╗██╔═══██╗████╗  ██║██╔════╝██║
██║  ██║██║   ██║██╔██╗ ██║█████╗  ██║
██║  ██║██║   ██║██║╚██╗██║██╔══╝  ╚═╝
██████╔╝╚██████╔╝██║ ╚████║███████╗██╗
╚═════╝  ╚═════╝ ╚═╝  ╚═══╝╚══════╝╚═╝"
