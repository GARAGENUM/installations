#!/bin/bash

echo "\n
████████╗ █████╗ ██████╗ ██████╗ ██╗   ██╗
╚══██╔══╝██╔══██╗██╔══██╗██╔══██╗╚██╗ ██╔╝
   ██║   ███████║██████╔╝██████╔╝ ╚████╔╝ 
   ██║   ██╔══██║██╔══██╗██╔══██╗  ╚██╔╝  
   ██║   ██║  ██║██████╔╝██████╔╝   ██║   
   ╚═╝   ╚═╝  ╚═╝╚═════╝ ╚═════╝    ╚═╝"

version=1.0.215

cd /tmp
wget -O - https://github.com/Eugeny/tabby/releases/download/v$version/tabby-$version-linux-x64.deb > tabby.deb
sudo dpkg -i ./tabby.deb
rm -rf ./tabby.deb
cd -

# cp ../files/config.yaml ~/.config/tabby

exit 0