#!/bin/bash

echo "\n
██╗   ██╗██╗██████╗ ████████╗██╗   ██╗ █████╗ ██╗     ██████╗  ██████╗ ██╗  ██╗
██║   ██║██║██╔══██╗╚══██╔══╝██║   ██║██╔══██╗██║     ██╔══██╗██╔═══██╗╚██╗██╔╝
██║   ██║██║██████╔╝   ██║   ██║   ██║███████║██║     ██████╔╝██║   ██║ ╚███╔╝ 
╚██╗ ██╔╝██║██╔══██╗   ██║   ██║   ██║██╔══██║██║     ██╔══██╗██║   ██║ ██╔██╗ 
 ╚████╔╝ ██║██║  ██║   ██║   ╚██████╔╝██║  ██║███████╗██████╔╝╚██████╔╝██╔╝ ██╗
  ╚═══╝  ╚═╝╚═╝  ╚═╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚══════╝╚═════╝  ╚═════╝ ╚═╝  ╚═╝"

# dependencies
sudo apt install libqt5opengl5

# vbox 7.0
wget https://download.virtualbox.org/virtualbox/7.0.14/virtualbox-7.0_7.0.14-161095~Debian~bookworm_amd64.deb
sudo dpkg -i virtualbox-*.deb

# extension pack
wget https://download.virtualbox.org/virtualbox/7.0.14/Oracle_VM_VirtualBox_Extension_Pack-7.0.14.vbox-extpack
VBoxManage extpack install *.vbox-extpack

# clean up 
rm -rf virtualbox-*.deb
rm -rf .vbox-extpack

exit 0