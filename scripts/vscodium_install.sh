#!/bin/bash

echo "\n
██╗   ██╗███████╗ ██████╗ ██████╗ ██████╗ ██╗██╗   ██╗███╗   ███╗   
██║   ██║██╔════╝██╔════╝██╔═══██╗██╔══██╗██║██║   ██║████╗ ████║    
██║   ██║███████╗██║     ██║   ██║██║  ██║██║██║   ██║██╔████╔██║   
╚██╗ ██╔╝╚════██║██║     ██║   ██║██║  ██║██║██║   ██║██║╚██╔╝██║ 
 ╚████╔╝ ███████║╚██████╗╚██████╔╝██████╔╝██║╚██████╔╝██║ ╚═╝ ██║
  ╚═══╝  ╚══════╝ ╚═════╝ ╚═════╝ ╚═════╝ ╚═╝ ╚═════╝ ╚═╝     ╚═╝"

version=1.94.2.24286

# INSTALL VSCODIUM FOR DEBIAN 11
sudo apt update -y
cd /tmp
wget -O - https://github.com/VSCodium/vscodium/releases/download/"$version"/codium_"$version"_amd64.deb > vscodium.deb
sudo apt-get install ./vscodium.deb
rm -rf ./vscodium.deb
cd -
exit 0
